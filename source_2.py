import cv2
import numpy as np
import argparse
import random as rnd

#############################################################################
#                           UTILITY FUNCTIONS                               #
#############################################################################


def fillflood_wall(img, flag=4):
    img_copy = img.copy()
    h, w, ch = img_copy.shape
    mask = np.zeros((h+2, w+2), np.uint8)   # floodfill wants a mask 2 px taller and wider
    largest_segment = 0

    for y in range(0, h):
        for x in range(0, w):
            # at start, mask is all zeroes; proceed to fill the mask if if finds an area with the same color
            if mask[y + 1][x + 1] == 0:
                new_val = (rnd.randint(0, 256), rnd.randint(0, 256), rnd.randint(0, 256))   # fill the area with rand color
                _, _, _, rect = cv2.floodFill(img_copy, mask, (x, y), new_val, (3, 3, 3), (11, 11, 11), flag)

                segment_size = rect[2] * rect[3]    # size of the homogenous area found
                if segment_size > largest_segment:
                    largest_segment = segment_size  # the wall will be the area with the largest size
                    wall_color = new_val

    dst = img.copy()
    dst = cv2.inRange(img_copy, wall_color, wall_color) # in the input image, fill the wall (largest area) with wall_color
    return dst


def contrast_brightness_enhance(img):
    # extract luminance information
    ycbcr_img = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    y = np.ndarray.flatten(ycbcr_img.swapaxes(0, 2)[0])
    median_y = np.uint8(np.round(np.median(y)))

    # apply transformation only if overall luminance is too low
    if median_y < 100:
        cv2.convertScaleAbs(img, img, alpha=1.5, beta=(100-median_y))
        if DEBUG:
            cv2.imshow("STEP #0: Contrast and brightness enhancement", img)
            cv2.waitKey(0)


def draw_bounding_boxes(original_image, componentsCorners):
    for c in componentsCorners:
        rect = np.zeros((4, 2), np.float32)
        s = np.sum(c, axis=1)
        rect[0] = c[np.argmin(s)]
        rect[2] = c[np.argmax(s)]
        d = np.diff(c, axis=1)
        rect[1] = c[np.argmin(d)]
        rect[3] = c[np.argmax(d)]
        (tl, tr, br, bl) = rect
        cv2.line(original_image, tuple(tl), tuple(tr), (0, 255, 0), 2, cv2.LINE_AA)
        cv2.line(original_image, tuple(tr), tuple(br), (0, 255, 0), 2, cv2.LINE_AA)
        cv2.line(original_image, tuple(br), tuple(bl), (0, 255, 0), 2, cv2.LINE_AA)
        cv2.line(original_image, tuple(bl), tuple(tl), (0, 255, 0), 2, cv2.LINE_AA)

    return original_image


def find_corners(binary_image):
    bn_image_float32 = np.float32(binary_image)
    dst = cv2.cornerHarris(bn_image_float32, 10, 9, 0.04)
    ret, dst = cv2.threshold(dst, 0.1 * dst.max(), 255, 0)  # threshold to eliminate some incorrect corners
    dst = np.uint8(dst)

    # refine corners position using 'cornerSubPix'
    ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
    corners = cv2.cornerSubPix(bn_image_float32, np.float32(centroids), (5, 5), (-1, -1), criteria)

    return corners


def find_connected_components(binary_image):
    contour_img = binary_image.copy()
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))
    contour_dilation = cv2.dilate(contour_img, kernel, iterations=1)
    ret, labels = cv2.connectedComponents(contour_dilation, 8)

    return labels


#############################################################################
#                               MAIN                                        #
#############################################################################

# command line arguments management
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help="path to the image file", required=True)
ap.add_argument("-d", "--debug", help="debug mode", type=bool, default=False)

args = vars(ap.parse_args())
DEBUG = args["debug"]
original_image = cv2.imread(args["image"])
if original_image is None:
    print("\n Errore nella lettura dell'immagine; controllare il path.\n")
    exit()

# PRE-PROCESSING
if original_image.shape[0] > 1080:
        original_image = cv2.resize(original_image, (0,0), fx=0.5, fy=0.5)
image = original_image.copy()
contrast_brightness_enhance(image)


# MEAN-SHIFT FILTERING
image = cv2.pyrMeanShiftFiltering(image, 5, 13, 1)
if DEBUG:
    cv2.imshow("STEP #1: Mean-shift filtering", image)
    cv2.waitKey(0)


# FILLFLOOD WALL MASK, to find wall
dst_4 = fillflood_wall(image)           # first fillflood with 4-way neigh.
dst_8 = fillflood_wall(image, flag=8)   # second fillflood with 8-way neigh.
image = cv2.bitwise_or(dst_4, dst_8)
image = cv2.bitwise_not(image)
if DEBUG:
    cv2.imshow("STEP #2: Floodfill wall mask", image)
    cv2.waitKey(0)


# REMOVE NOISE
kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(18,18))
image = cv2.morphologyEx(image,cv2.MORPH_OPEN,kernel)
if DEBUG:
    cv2.imshow("STEP #3: Morphological open", image)
    cv2.waitKey(0)


# KEEP ONLY RECTANGULAR / SQUARE CONTOURS
hull_image = np.zeros((image.shape[0], image.shape[1]), np.uint8)
hull = []

_, contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
for i in range(len(contours)):
    # approximate contour
    peri = cv2.arcLength(contours[i], True)
    approx = cv2.approxPolyDP(contours[i], 0.03 * peri, True)

    # if approximated contour has 4 vertices it's a painting candidate, otherwise it's canceled (-> black)
    if len(approx) == 4:
        hull.append(cv2.convexHull(contours[i], True, True))

cv2.drawContours(hull_image, hull, -1, (255, 255, 255), cv2.FILLED)
if DEBUG:
    cv2.imshow("STEP #4: Convex Hull on 4 vertices polygons", hull_image)
    cv2.waitKey(0)


# FILTER OUT POLYGONS WITH SMALL AREAS AND CORNER DETECTION
_, contours, _ = cv2.findContours(hull_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
areas = [cv2.contourArea(c) for c in contours]
if len(areas) > 0:
    max_area = areas[np.argmax(areas)]
    mean_area = np.mean(areas)

componentsCorners = []  # array of connected components with their 4 corners
boundingRects = []      # array of bounding rectangles (-> useful for warping in the future)

for i in range(len(contours)):
    if areas[i] > (mean_area * 0.238):
        #draw and work on the contour only if its area is above the threshold
        cv2.drawContours(hull_image, contours, i, (255,255,255), cv2.FILLED)

        # create black image with only selected contour (-> for extracting only its corners)
        tmp_image = np.zeros((original_image.shape[0], original_image.shape[1]), np.uint8)
        cv2.drawContours(tmp_image, contours, i, (255, 255, 255), cv2.FILLED)

        # find corners
        opened = np.float32(tmp_image)
        dst = cv2.cornerHarris(opened, 10, 9, 0.04)
        ret, dst = cv2.threshold(dst, 0.1 * dst.max(), 255, 0)
        dst = np.uint8(dst)

        # refine corners
        ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
        corners = cv2.cornerSubPix(opened, np.float32(centroids), (5, 5), (-1, -1), criteria)

        # insert corners of the single component in the global components array
        component_corners = []
        HarrisCorners = corners[1:]  # remove first corner (-> it's always the centroid of the entire image)
        for x, y in HarrisCorners:
            x_int = np.uint32(np.round(x))
            y_int = np.uint32(np.round(y))
            component_corners.append((x, y))

        # remove components with num_of_corners != 4
        if len(component_corners) == 4:
            # append corners to global components corners array
            componentsCorners.append(component_corners)

            # extract its bounding rect and append it to the global bounding boxes array
            rect = cv2.boundingRect(contours[i])
            x, y, w, h = rect
            tl = (x, y)
            tr = (x + w, y)
            br = (x + w, y + h)
            bl = (x, y + h)

            boundingRects.append([tl, tr, br, bl])
    else:
        # if area below threshold, remove contour (-> fill it with background color)
        cv2.drawContours(hull_image, contours, i, (0,0,0), cv2.FILLED)

if DEBUG:
    cv2.imshow("STEP #5: Eliminate polygons with small areas", hull_image)
    cv2.waitKey(0)

if DEBUG:
    tmp = original_image.copy()
    for c in componentsCorners:
        for x,y in c:
            cv2.circle(tmp, (x, y), 1, (0, 255, 0), 4)
    cv2.imshow("STEP #6: Corners and labeling", tmp)
    cv2.waitKey(0)


# DRAW BOUNDING BOXES by connecting the 4 corners of a single connected component
paintings_detected = original_image.copy()
paintings_detected = draw_bounding_boxes(original_image.copy(), componentsCorners)

cv2.destroyAllWindows()
cv2.imshow("Paintings detected", paintings_detected)
cv2.waitKey(0)

# PAINTINGS' RECTIFICATION
copy_original = original_image.copy()
warped_paintings = []

for i in range(len(componentsCorners)):
    comp = np.asarray(componentsCorners[i])
    rect = np.zeros((4, 2), np.float32)
    s = comp.sum(axis=1)
    rect[0] = comp[np.argmin(s)]
    rect[2] = comp[np.argmax(s)]
    d = np.diff(comp, axis=1)
    rect[1] = comp[np.argmin(d)]
    rect[3] = comp[np.argmax(d)]

    (tl, tr, br, bl) = rect
    (b_tl, b_tr, b_br, b_bl) = boundingRects[i]

    w = b_tr[0] - b_tl[0]
    h = b_br[1] - b_tr[1]

    dst = np.array([[0, 0], [w - 1, 0], [w - 1, h - 1], [0, h - 1]], dtype="float32")

    M = cv2.getPerspectiveTransform(rect, dst)
    warp = cv2.warpPerspective(copy_original, M, (w, h))
    warped_paintings.append(warp)

for i in range(len(warped_paintings)):
    cv2.imshow("FINAL STEP: Warped painting #" + str(i), warped_paintings[i])
    cv2.waitKey(0)
cv2.destroyAllWindows()
